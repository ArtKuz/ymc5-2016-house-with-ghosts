'use strict';
angular.module('main')
  .directive('wavesurfer', function ($interval, $window) {
    var uniqueId = 1;
    return {
      restrict : 'AE',
      scope    : {
        url     : '=',
        options : '='
      },
      templateUrl: 'main/templates/wavesurfer.html',
      link : function (scope, element) {
        var id = uniqueId++;
        scope.uniqueId = 'waveform_' + id;
        scope.wavesurfer = Object.create(WaveSurfer);
        scope.playing    = false;
        scope.volume_level = ($window.sessionStorage.audioLevel || 50);

        var waveform = element.children()[0].children[0].children[4];

        // initialize the wavesurfer
        scope.options = _.extend({container: waveform}, scope.options);
        scope.wavesurfer.init(scope.options);
        scope.wavesurfer.load(scope.url);
        scope.moment = "0";
        // on ready
        scope.wavesurfer.on('ready', function () {
          scope.length = Math.floor(scope.wavesurfer.getDuration()).toString();
          $interval(function () {
            scope.moment = Math.floor(scope.wavesurfer.getCurrentTime()).toString();
          }, parseFloat(scope.playrate) * 1000);
        });
        // what to be done on finish playing
        scope.wavesurfer.on('finish', function () {
          scope.playing = false;
        });
        // play/pause action
        scope.playpause = function () {
          scope.wavesurfer.playPause();
          scope.playing = !scope.playing;
        };

        scope.ff = function () {
          scope.wavesurfer.skipForward();
        };

        scope.bw = function () {
          scope.wavesurfer.skipBackward();
        };
      }
    };
  });
