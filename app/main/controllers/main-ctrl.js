'use strict';
angular.module('main')
.controller('MainCtrl', function ($scope) {

  $scope.options = {
    waveColor      : '#c5c1be',
    progressColor  : '#2A9FD6',
    normalize      : true,
    hideScrollbar  : true,
    skipLength     : 15,
    height         : 53,
    cursorColor    : '#2A9FD6'
  };

  $scope.url = 'main/assets/herloksholms.wav';

})


