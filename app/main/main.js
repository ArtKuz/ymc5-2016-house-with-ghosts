'use strict';
angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
])
.config(function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/main');
  $stateProvider
    .state('main', {
      url: '/main',
      cache: false,
      templateUrl: 'main/templates/main.html',
      controller: 'MainCtrl as vm'
    })
});
